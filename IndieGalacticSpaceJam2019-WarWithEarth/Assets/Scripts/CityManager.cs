﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityManager : MonoBehaviour
{
    public City[] Cities;

    public Projectile Projectile;

    public delegate void CityManagerEvent(string CityName, int Pop);
    public event CityManagerEvent CityHit;

    // Start is called before the first frame update
    void Start()
    {
        //foreach (City c in Cities)
        //{
        //    c.CityHit += OnCityHit;
        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCityHit(string cityName, int pop)
    {
        Debug.Log($"City Hit {cityName} with population {pop}");

        CityHit(cityName, pop);
    }

    void OnProjectileDestroyed()
    {

    }
}
