﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour
{
    private Light flasher;

    // Start is called before the first frame update
    void Start()
    {
        flasher = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        flasher.intensity = Mathf.Lerp(0, 100, Mathf.PingPong(Time.time, .5f));
    }
}
