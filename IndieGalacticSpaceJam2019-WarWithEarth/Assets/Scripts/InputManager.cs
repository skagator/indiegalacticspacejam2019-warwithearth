﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    public float PowerMultiplier;
    public float Power;
    public GameObject Projectile;
    public GameObject Camera;
    [Range(-1, 0)]
    public float ShakeThreshold;
    public bool projectileFired = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float inputVal = Input.GetAxis("Vertical");
        SetPower(inputVal);
        if (inputVal < ShakeThreshold && projectileFired == false)
        {
            Camera.GetComponent<CameraShake>().StartShake();
        }
        else if (inputVal > ShakeThreshold)
        {
            Camera.GetComponent<CameraShake>().StopShake();
        }
        if (Input.GetButton("Fire1"))
        {
            Fire();
            Camera.GetComponent<CameraShake>().StopShake();
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    void SetPower(float powerVal)
    {
        Power = powerVal * -1 * PowerMultiplier;
    }

    void Fire()
    {
        Projectile.GetComponent<Projectile>().FireProjectile(Power);
        projectileFired = true;
    }
}
