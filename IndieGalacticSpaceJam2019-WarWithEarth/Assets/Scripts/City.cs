﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : MonoBehaviour
{
    Collider cityCollider;
    public int Population;
    public string CityName;

    public delegate void CityEvent(string city, int population);
    public event CityEvent CityHit;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(CityHit != null)
            CityHit(CityName, Population);
    }
}
