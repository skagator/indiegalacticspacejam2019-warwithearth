﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public delegate void FireEvent();
    public event FireEvent OnProjectileFired;

    public delegate void ProjectileEvent(City hitCity);
    public event ProjectileEvent ProjectileDestroyed;

    public Rigidbody rb;

    public bool Fired = false;

    public GameObject Earth;
    public float EarthGravity = 0f;

    public float EarthGravityMin = 0.1f;
    public float EarthGravityMax = 10.0f;
    private float distanceToEarth = 0f;
    private float totalDistanceToEarth = 0.0f;

    public float projectileFireSpeed;
    public float projectileVelocity;

    public float TurnSpeed;
    public float LookAtSpeed;

    public float rightSpeed;
    public float upSpeed;

    public MeshRenderer rockRenderer;

    public GameObject explosionPrefab;

    public float ProjectileLife = 60f;

    // Start is called before the first frame update
    void Start()
    {
        totalDistanceToEarth = (transform.position - Earth.transform.position).magnitude;
    }

    // Update is called once per frame
    void Update()
    {
        projectileVelocity = rb.velocity.magnitude;
        distanceToEarth = (transform.position - Earth.transform.position).magnitude;
        EarthGravity = Mathf.Clamp(EarthGravityMax * distanceToEarth/totalDistanceToEarth, EarthGravityMin, EarthGravityMax);

        if (!Fired)
        {
            if (Input.GetAxis("RightH") != 0)
            {
                transform.Rotate(0f, Input.GetAxis("RightH"), 0f, Space.World);
            }
            if (Input.GetAxis("RightV") != 0)
            {
                transform.Rotate(Input.GetAxis("RightV"), 0f, 0f, Space.World);
            }
        }
        else
        {
            float rightVal = Input.GetAxis("RightH") * rightSpeed;
            float upVal = Input.GetAxis("RightV") * upSpeed * -1;

            rb.AddForce(new Vector3(rightVal, upVal, 0), ForceMode.Acceleration);

            if(ProjectileLife <= 0.0f)
            {
                Debug.Log("Time Expired");
                DestroyProjectile(null);
            }
            else
            {
                ProjectileLife -= Time.deltaTime;
            }
        }

        //transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x, -45, -25), y, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        City hitCity = collision.gameObject.GetComponent<City>();

        DestroyProjectile(hitCity);
    }

    private void DestroyProjectile(City hitCity)
    {
        ProjectileDestroyed(hitCity);

        GameObject.Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        
        GameObject.Destroy(this.gameObject);

    }

    private void FixedUpdate()
    {
        if (Fired)
        {
            Vector3 dir = (Earth.transform.position - transform.position);
            rb.AddForce(dir.normalized * EarthGravity, ForceMode.Acceleration);

            Quaternion toRotation = Quaternion.FromToRotation(transform.forward, dir);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, LookAtSpeed * Time.time);
        }
    }

    public void FireProjectile(float force)
    {
        if (Fired == false)
        {
            rb.AddRelativeForce(rb.transform.forward * force * projectileFireSpeed);
            if (OnProjectileFired != null)
                OnProjectileFired();
            Fired = true;
        }
    }

}
