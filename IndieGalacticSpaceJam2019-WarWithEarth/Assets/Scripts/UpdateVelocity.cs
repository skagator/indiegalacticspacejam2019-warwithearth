﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateVelocity : MonoBehaviour
{
    public Text velocityText;
    public Projectile projectile;
    public float textSpeedMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        velocityText.text = string.Format("{0} m/s", (projectile.projectileVelocity * textSpeedMultiplier).ToString("n2"));
    }
}
