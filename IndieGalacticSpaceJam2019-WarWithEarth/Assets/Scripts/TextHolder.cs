﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextHolder : MonoBehaviour
{
    public Camera mainCamera;
    private TextMesh cityname;
    public GameObject city;

    // Start is called before the first frame update
    void Start()
    {
        cityname = GetComponent<TextMesh>();
        cityname.text = city.name;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(2 * transform.position - mainCamera.transform.position);
    }
}
