﻿///Daniel Moore (Firedan1176) - Firedan1176.webs.com/
///26 Dec 2015
///
///Shakes camera parent object

using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public Transform camTransform;

    // How long the object should shake for.
    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0f;
    public float decreaseFactor = 0f;
    public bool isShaking = false;

    Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }
    }

    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }

    void Update()
    {
        if (isShaking)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            //shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        //else
        //{
        //    //shakeDuration = 0f;
        //    camTransform.localPosition = originalPos;
        //}
    }

    public void StartShake()
    {
        isShaking = true;
    }

    public void StopShake()
    {
        isShaking = false;
    }
}