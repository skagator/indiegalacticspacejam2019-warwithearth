﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Earth;

    public Transform targetObject;
    private Vector3 followOffset;

    public bool isFollowing;

    public float LookAtSpeed;

    private bool fired = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindObjectOfType<CameraTrigger>().CameraTriggered += OnCameraTriggered;
        GameObject.FindObjectOfType<Projectile>().OnProjectileFired += OnProjectileFired;
    }

    // Update is called once per frame
    void Update()
    {
        if(fired)
        {
            Vector3 dir = Earth.transform.position - transform.position;
            Quaternion toRotation = Quaternion.FromToRotation(transform.forward, dir);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, LookAtSpeed * Time.deltaTime);
        }
    }

    void StopFollowing()
    {
        transform.SetParent(null);
        isFollowing = false;
    }

    void StartFollowing()
    {
        transform.SetParent(GameObject.FindObjectOfType<Projectile>().transform);
        isFollowing = true;
    }

    void OnCameraTriggered()
    {
        StopFollowing();
    }

    void OnProjectileFired()
    {
        StartFollowing();
        fired = true;
    }
}
