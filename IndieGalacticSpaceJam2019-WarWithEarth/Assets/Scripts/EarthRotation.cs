﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthRotation : MonoBehaviour
{
    public float rotateSpeed = .1f;

    // Start is called before the first frame update
    void Start()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0f, 360f), transform.eulerAngles.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,-rotateSpeed,0,Space.World);
    }
}
