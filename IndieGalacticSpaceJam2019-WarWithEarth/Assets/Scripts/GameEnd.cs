﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameEnd : MonoBehaviour
{

    public GameObject Panel;

    public CityManager CM;


    public Text TopMessage;
    public Text MiddleMessage;
    public Text BottomMessage;

    public Projectile projectile;


    // Start is called before the first frame update
    void Start()
    {
        projectile.ProjectileDestroyed += OnProjectileDestroyed;
        //CM.CityHit += OnCityHit;
    }

    // Update is called once per frame
    void Update()
    {
        if(Panel.activeSelf == true)
        {
            if(Input.GetButtonDown("Fire1"))
            {
                //Reload Scene
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
            }
        }
    }

    void UpdateMessageCityHit(string cityName, int Pop)
    {
        TopMessage.text = $"Destroyed {cityName}";
        MiddleMessage.text = "Population:";
        BottomMessage.text = String.Format("{0:n0}", Pop);
    }

    void UpdateMessageMissed()
    {
        TopMessage.text = "Missed";
        MiddleMessage.text = "";
        BottomMessage.text = "";
    }

    void OnCityHit(string cityName, int pop)
    {
        Panel.SetActive(true);
        UpdateMessageCityHit(cityName, pop);
    }

    void OnProjectileDestroyed(City hitCity)
    {
        Panel.SetActive(true);

        if (hitCity != null)
        {
            UpdateMessageCityHit(hitCity.CityName, hitCity.Population);
        }
        else
        {
            UpdateMessageMissed();
        }
    }
}
